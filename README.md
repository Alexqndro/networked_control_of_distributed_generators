# Networked_control_of_distributed_generators

## Description
Control of a Power Network System (PNS) composed of several power generation areas coupled through tie-lines. The classical centralized structure is compared with distributed and decentralized structures.

## Authors and acknowledgment
1. Del Duca Alessandro
2. Ferrario Alex
3. Pajni Giacomo
4. Specchia Simone

## Repository branches 
0. master
1. testing

## Abstract
Control of a Power Network System (PNS) composed of several power generation areas coupled through tie-lines. The classical centralized structure is compared with distributed and decentralized structures in both discrete and continuous time, using the
First, the open-loop analysis is performed, enlighten the presence of an integrator in the system. Then the system is controlled in a classical centralized way.
After checking the absence of decentralized and distributed fixed modes through spectral radius, the control gains are computed through LMI's while imposing the network topology.
Finaly a comparison between the performances of the ideal (under a pure control perspective) centralized structure and the decentralized and distributed structure is performed.

## Report
The report can be found at the link: https://gitlab.com/Alexqndro/networked_control_of_distributed_generators/-/blob/main/Multi_area_power_network_systems.pdf

## Folders
- Networked_functions: This folder contain the functions to compute the spectral abscissa and fixed modes of the problem

## Usage
Run the main script "networked_main.m" that compute the differents topologies of control both in continuous and discrete time.

### Topologies
1. Centralized
    - Pole placement
    - LQR
    - LMI with constrained spectral abscissa
2. Decentralized
3. Distributed
    - Unidirectional string
    - Bidirectional string 
    - Star centered in node 1 
    - Start centered in node 2 
    - Physical based 

## Support & Contributing
For any questions and suggestions, or if you want to contribute, write to alessandrodelduca.96@gmail.com

## License
All the code in the repository is under GNU General public License 3.0v 

## Project status
Maintaining
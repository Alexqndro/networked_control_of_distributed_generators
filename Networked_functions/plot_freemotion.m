function plot_freemotion(Acl,Fcl,dt,x0,title1)

t=0:dt/100:50;
for k=1:length(t)
Xresp_ct(:,k) = expm(Acl*t(k))*x0;
end

td=0:dt:50;
for ii = 1:length(td)
Xresp_dt(:,ii) = Fcl^(ii-1)*x0;
end

figure;
subplot(2,1,1);plot(t,Xresp_ct([1,2,3,4],:))
title(title1)
subtitle('First subsystem Continuous Time');
legend('\Delta\theta_1','\Delta\omega_1','\DeltaP_{m1}','\DeltaP{v1}','location','northeast');

subplot(2,1,2);stairs(td,Xresp_dt([1,2,3,4],:)');
title(title1)
subtitle('First subsystem Discrete Time');
legend('\Delta\theta_1','\Delta\omega_1','\DeltaP_{m1}','\DeltaP{v1}','location','northeast');

figure;
subplot(2,1,1);plot(t,Xresp_ct([2,6,10,14,18],:))
title(title1)
subtitle('\Delta\omega_i Continuous Time');
legend('\Delta\omega_1','\Delta\omega_2','\Delta\omega_3','\Delta\omega_4','\Delta\omega_5','location','northeast');

subplot(2,1,2);stairs(td,Xresp_dt([2,6,10,14,18],:)');
title(title1)
subtitle('\Delta\omega_i Discrete Time');
legend('\Delta\omega_1','\Delta\omega_2','\Delta\omega_3','\Delta\omega_4','\Delta\omega_5','location','northeast');

end

function [FM_ct,FM_dt,Kstab_ct,Kstab_dt,rho_ct,rho_dt,Acl,Fcl] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,N,ContStruct,n_round,x0,dt,title)

FM_ct = di_fixed_modes(A,Bdec,Cdec,N,ContStruct,n_round);
FM_dt = di_fixed_modes(F,Gdec,Hdec,N,ContStruct,n_round);


[Kstab_ct,rho_ct,feas] = LMI_CT_DeDicont(A,Bdec,Cdec,N,ContStruct);
display(feas);

[Kstab_dt,rho_dt,feas] = LMI_DT_DeDicont(F,Gdec,Hdec,N,ContStruct);

display(feas);  


Btot=[];
Gtot=[];
for i=1:N
    Btot=[Btot,Bdec{i}];
    Gtot=[Gtot,Gdec{i}];
end
Acl = A+Btot*Kstab_ct;
Fcl = F+Gtot*Kstab_dt;

plot_freemotion(Acl,Fcl,dt,x0,title)
end
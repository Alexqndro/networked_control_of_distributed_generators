function [K,rho,feas]=LMI_CT_eig(Atot,Bdec,Cdec,N,ContStruc)
% Computes, using LMIs, the distributed "state feedback" control law for the continuous-time system, with reference to the control
% information structure specified by 'ContStruc'.
%
% Inputs:
% - Atot: system matrix.
% - Bdec: input matrices (i.e., Bdec{1},..., Bdec{N} are the input matrices of the decomposed system, one for each channel).
% - Cdec: output matrices  (i.e., Cdec{1},..., Cdec{N} are the output matrices of the decomposed system, one for each channel, where [Cdec{1}',...,
% Cdec{N}']=I).
% - N: number of subsystems.
% - ContStruc: NxN matrix that specifies the information structure
% constraints (ContStruc(i,j)=1 if communication is allowed between channel
% j to channel i, ContStruc(i,j)=0 otherwise).
%
% Output:
% - K: structured control gain
% - rho: spectral abscissa of matrix (A+B*K) - note that [Cdec{1}',...,
% Cdec{N}']=I
% - feas: feasibility of the LMI problem (=0 if yes)

Btot=[];
Ctot=[];
for i=1:N
    m(i)=size(Bdec{i},2);
    n(i)=size(Cdec{i},1);
    Btot=[Btot,Bdec{i}];
    Ctot=[Ctot;Cdec{i}];
end
ntot=size(Atot,1);
mtot=sum(m);

yalmip clear

if ContStruc==ones(N,N)
    % Centralized design
    P=sdpvar(ntot);
    l=sdpvar(1);  
end
M = [-Atot'*P-P*Atot-Ctot'*Ctot, P*Btot;   
            Btot'*P                l*eye(mtot);];
LMIconstr=[M >=1e-1*eye(ntot+mtot)] +[P>=1e-1*eye(ntot)];
options=sdpsettings('solver','sedumi');
J=optimize(LMIconstr,l,options);
feas=J.problem;
l=double(l);
P=double(P);

K=l/P;
rho=max(real(eig(Atot+Btot*K)));

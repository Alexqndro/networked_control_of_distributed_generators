clearvars; close all; 
set(0, 'DefaultFigureWindowStyle', 'normal')
clc;
clear('yalmip');

addpath('Networked_functions')
Multi_area_matrices;

% Parameters
samplingTime = 1;
% samplingTime = 2;
% samplingTime = 0.1;
window = 5;

% Initial conditions
x0 = [9.7209; 0.3146;
      8.3540; 8.3571; 
      0.4986; 5.4589; 
      9.4317; 3.2147; 
      8.0647; 6.0140; 
      7.8962; 7.9919; 
      0.4956; 2.8320; 
      6.5346; 4.8966; 
      9.7285; 7.4849; 
      5.6784; 2.9896];

% Open Loop system 
% Continuous time
sys_cont            = ss(A,B,C,0);
[eigvecCT,eigvalCT] = eig(A);
rho_cont            = round(max(real(eig(A))),3);
roots_cont          = roots(poly(A));

% Discrete time
% Exact form
F_exact = expm(A*samplingTime);
G_exact = -inv(A)*(eye(20) - F_exact)*B;
% ZOH form
sys_disc            = c2d(sys_cont,samplingTime,'zoh');
[F,G,H]             = ssdata(sys_disc);
[eigvecDT,eigvalDT] = eig(F);
rho_disc            = max(abs(eig(F)));
roots_disc          = roots(poly(F));

title = 'Openloop free-motion';
plot_ol_freemotion(A,F,samplingTime,x0,title);

%% Open Loop Stability analysis with Yalmip
n = 20;
P = sdpvar(n);
L = [A'*P+P*A<=-0.01*eye(n)]+[P>=0.01*eye(n)];
results_cont = optimize(L);

yalmip clear
P=sdpvar(n);
L=[F'*P*F-P<=-eye(n)]+[P>=eye(n)];
results_disc=optimize(L);


%Matrix decomposition
for k=1:window
    Bdec{k} = B(:,k); 
    Cdec{k} = C((4*(k-1)+1):(4*k),:); 
    Gdec{k} = G(:,k); 
    Hdec{k} = H((4*(k-1)+1):(4*k),:);     
    
end

%% Punto 3
e = eig(A);
for i = 1:20
pbh(i) = rank([e(i)*eye(20) - A ,B]);
end

% Controllo centralizzato
ContStruct = ones(window,window);
title = 'Centralized control with LMIs';
[CFMc1,CFMd1,K_centr_c1,K_centr_d1,rho_centr_c1,rho_centr_d1] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

%Pole placement
p   = [-0.5 -0.6 -0.3 -0.55 -0.5 -0.6 -0.3 -0.55 -0.5 -0.6 -0.3 -0.55 -0.5 -0.6 -0.3 -0.55 -0.5 -0.6 -0.35 -0.55 ];
Kc  = place(A,B,p);
p   = [0.5 0.55 0.65 0.7 0.5 0.55 0.65 0.7 0.5 0.55 0.65 0.7 0.5 0.55 0.65 0.7 0.5 0.55 0.65 0.7 ];
Kd  = place(F,G,p);
App = A - B*Kc;
Fpp = F - G*Kd;
title = 'Centralized control with Pole placement';
plot_freemotion(App,Fpp,samplingTime,x0,title);
rho_pp_c = max(real(eig(App)));
rho_pp_d = max(abs(eig(Fpp)));

% LQR 
Q     = 1.9*eye(20);
R     = 0.4*eye(5);
Klqrc = lqr(A,B,Q,R);
Klqrd = dlqr(F,G,Q,R);
Alqr  = A - B*Klqrc;
Flqr  = F - G*Klqrd;
title = 'Centralized control with LQR';
plot_freemotion(Alqr,Flqr,samplingTime,x0,title);

rho_lqr_c = max(real(eig(Alqr)));
rho_lqr_d = max(abs(eig(Flqr)));

% LMI with moved poles
A = A + 0.2*eye(20);
F = F/0.8;
t = 0:0.1:10;
ContStruct = ones(window,window);
title = 'IGNORE THIS';
[CFMc2,CFMd2,K_centr_c2,K_centr_d2,rho_centr_c2,rho_centr_d2] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

A   = A - 0.2*eye(20);
F   = F*0.8;
Acl = A + B*K_centr_c2;
Fcl = F + G*K_centr_d2;
title = 'Centralized control with LMIs';
plot_freemotion(Acl,Fcl,samplingTime,x0,title);
rho_centr_c2 = max(real(eig(Acl)));
rho_centr_d2 = max(abs(eig(Fcl)));

% Controllo decentralizzato
ContStruct = eye(window);
title = 'Decentralized control';
[DecFMc,DecFMd,K_decen_c,K_decen_d,rho_decen_c,rho_decen_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito String Unidirectional 1
ContStruct = [1,0,0,0,0;
              1,1,0,0,0;
              0,1,1,0,0;
              0,0,1,1,0;
              0,0,0,1,1];
title = 'Distribuited control: Unidirectional string l->r';
[D_StrUn1_FMc,D_StrUn1_FMd,K_StrUn1_c,K_StrUn1_d,rho_StrUn1_c,rho_StrUn1_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito String Unidirectional 2
ContStruct = [1,1,0,0,0;
              0,1,1,0,0;
              0,0,1,1,0;
              0,0,0,1,1;
              0,0,0,0,1];
title = 'Distribuited control: Unidirectional string r->l';
[D_StrUn2_FMc,D_StrUn2_FMd,K_StrUn2_c,K_StrUn2_d,rho_StrUn2_c,rho_StrUn2_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito String Bidirectional
ContStruct = [1,1,0,0,0;
              1,1,1,0,0;
              0,1,1,1,0;
              0,0,1,1,1;
              0,0,0,1,1];
title = 'Distribuited control: Bidirectional string';
[D_StrBi_FMc,D_StrBi_FMd,K_StrBi_c,K_StrBi_d,rho_StrBi_c,rho_StrBi_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito cycle Unidirectional 1
ContStruct = [1,0,0,0,1;
              1,1,0,0,0;
              0,1,1,0,0;
              0,0,1,1,0;
              0,0,0,1,1];
title = 'Distribuited control: Unidirectional cycle l->r';
[D_CycUn1_FMc,D_CycUn1_FMd,K_CycUn1_c,K_CycUn1_d,rho_CycUn1_c,rho_CycUn1_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito cycle Unidirectional 2
ContStruct = [1,1,0,0,0;
              0,1,1,0,0;
              0,0,1,1,0;
              0,0,0,1,1;
              1,0,0,0,1];
title = 'Distribuited control: Unidirectional cycle r->l';
[D_CycUn2_FMc,D_CycUn2_FMd,K_CycUn2_c,K_CycUn2_d,rho_CycUn2_c,rho_CycUn2_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito cycle Bidirectional
ContStruct = [1,1,0,0,1;
              1,1,1,0,0;
              0,1,1,1,0;
              0,0,1,1,1;
              1,0,0,1,1];
title = 'Distribuited control: Bidirectional cycle';
[D_CycBi_FMc,D_CycBi_FMd,K_CycBi_c,K_CycBi_d,rho_CycBi_c,rho_CycBi_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Custom topology
ContStruct = [1,1,0,0,0;
              1,1,1,0,1;
              0,1,1,1,0;
              0,0,1,1,1;
              0,1,0,1,1];
title = 'Distribuited control: physical topology';
[D_cust_FMc,D_cust_FMd,K_cust_c,K_cust_d,rho_cust_c,rho_cust_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);

% Controllo  Distribuito star 

ContStruct1 = [1,1,1,1,1;
               1,1,0,0,0;
               1,0,1,0,0;
               1,0,0,1,0;
               1,0,0,0,1];
ContStruct2 = [1,1,0,0,0;
               1,1,1,1,1;
               0,1,1,0,0;
               0,1,0,1,0;
               0,1,0,0,1];
ContStruct3 = [1,0,1,0,0;
               0,1,1,0,0;
               1,1,1,1,1;
               0,0,1,1,0;
               0,0,1,0,1];
ContStruct4 = [1,0,0,1,0;
               0,1,0,1,0;
               0,0,1,1,0;
               1,1,1,1,1;
               0,0,0,1,1];
ContStruct5 = [1,0,0,0,1;
               0,1,0,0,1;
               0,0,1,0,1;
               0,0,0,1,1;
               1,1,1,1,1];
ContStruct = ContStruct2;
title = 'Distribuited control: Star topology';
[D_star_FMc,D_star_FMd,K_star_c,K_star_d,rho_star_c,rho_star_d] = analyze_control_struct(A,Bdec,Cdec,F,Gdec,Hdec,window,ContStruct,3,x0,samplingTime,title);
